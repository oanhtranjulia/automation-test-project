@smoke_test
Feature: Inventory List Page tests
  *** https://www.saucedemo.com/

  Background:
    Given user is successfully login by config credentials to Swag Labs

  @inventory_list
  Scenario: Sort Inventory List
    Then display Inventory Page with inventory list
    And inventory list is sorted by Name (A to Z)
    When user selects sort by 'Name (Z to A)'
    Then inventory list is sorted by Name (Z to A)
    When user selects sort by 'Price (low to high)'
    Then inventory list is sorted by Price (low to high)
    When user selects sort by 'Price (high to low)'
    Then inventory list is sorted by Price (high to low)
    Then close the browser

