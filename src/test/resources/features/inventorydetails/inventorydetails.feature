@smoke_test
Feature: Inventory Details Page tests
  *** https://www.saucedemo.com/

  Background:
    Given user is successfully login by config credentials to Swag Labs

  @inventory_details
  Scenario: View Inventory Details
    Then display Inventory Page with inventory list
    When user clicks on an Inventory item 'Sauce Labs Backpack'
    Then display correct Inventory Details:
      | name        | Sauce Labs Backpack                                                                                                                    |
      | description | carry.allTheThings() with the sleek, streamlined Sly Pack that melds uncompromising style with unequaled laptop and tablet protection. |
      | price       | $29.99                                                                                                                                 |
    Then close the browser