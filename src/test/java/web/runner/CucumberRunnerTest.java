package web.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/***
 * <p> Cucumber Junit Test runner class. </p>
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features",
        glue = {"web.stepdefinitions"}
)
public class CucumberRunnerTest { }
