package web.loggermanager;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class LoggerManager {
  static final Logger logger = LogManager.getLogger("automation");

  public static void logInfo(String message) {
    logger.info(message);
  }

  public static void logError(String message) {
    logger.error(message);
  }

  public static void logBrowserStart(String browser) {
    StringBuilder logMessage = new StringBuilder("Launching Browser: ");
    logger.info(logMessage.append(browser));
  }

  public static void logFindElement(String locatorMethod, String locator) {
    StringBuilder logMessage = new StringBuilder("Find element: ");
    logger.info(logMessage.append("By ").append(locatorMethod).append(": ").append(locator));
  }

  public static void logInput(WebElement element, String data) {
    StringBuilder logMessage = new StringBuilder("Input data: ");
    logger.info(logMessage.append(data)
        .append(" | into element name: ")
        .append(element.getAttribute("name"))
        .append(" | element id: ")
        .append(element.getAttribute("id")));
  }

  public static void logClick(WebElement element) {
    StringBuilder logMessage = new StringBuilder("Click element: ");
    logger.info(logMessage
        .append("element name: ")
        .append(element.getAttribute("name"))
        .append(" | element id: ")
        .append(element.getAttribute("id"))
    );
  }

  public static void logSelect(WebElement element, String method) {
    StringBuilder logMessage = new StringBuilder("Select dropdown: ");
    logger.info(logMessage
        .append("element name: ")
        .append(element.getAttribute("name"))
        .append(" | element id: ")
        .append(element.getAttribute("id"))
        .append("By ").append(method));
  }

  public static void logVerify(String message, boolean result) {
    logInfo(message + ": " + result);
  }
}
