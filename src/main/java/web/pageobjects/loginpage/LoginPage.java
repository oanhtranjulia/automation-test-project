package web.pageobjects.loginpage;

import static web.pageobjects.base.model.SimplePage.clickElement;
import static web.pageobjects.base.model.SimplePage.findElementById;
import static web.pageobjects.base.model.SimplePage.inputData;

import org.openqa.selenium.WebDriver;

/***
 * <p> Login page page object. </p>
 */
public class LoginPage {
  WebDriver driver;
  private String url = "https://www.saucedemo.com";

  public LoginPage(WebDriver driver) {
    this.driver = driver;
    driver.get(url);
  }

  /***
   * <p> Login to the application. </p>
   *
   * @param username username
   * @param password password
   */
  public void loginSwagLabs(String username, String password) {
    inputData(findElementById("user-name"), username);
    inputData(findElementById("password"), password);
    clickElement(findElementById("login-button"));
  }
}
