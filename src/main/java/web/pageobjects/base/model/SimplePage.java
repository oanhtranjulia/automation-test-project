package web.pageobjects.base.model;

import static web.loggermanager.LoggerManager.logSelect;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import web.loggermanager.LoggerManager;

public class SimplePage {
  static WebDriver driver;

  public SimplePage(WebDriver driver) {
    SimplePage.driver = driver;
  }

  public static WebElement findElementById(String id) {
    try {
      LoggerManager.logFindElement("id", id);
      return driver.findElement(By.id(id));
    } catch (StaleElementReferenceException e) {
      LoggerManager.logError(e.getMessage());
      LoggerManager.logInfo("Retry");
      return driver.findElement(By.id(id));
    }
  }

  public static WebElement findElementByXpath(String xpath) {
    try {
      LoggerManager.logFindElement("xpath", xpath);
      return driver.findElement(By.xpath(xpath));
    } catch (StaleElementReferenceException e) {
      LoggerManager.logError(e.getMessage());
      LoggerManager.logInfo("Retry");
      return driver.findElement(By.xpath(xpath));
    }
  }

  public static WebElement findElementByCss(String css) {
    try {
      LoggerManager.logFindElement("css", css);
      return driver.findElement(By.cssSelector(css));
    } catch (StaleElementReferenceException e) {
      LoggerManager.logError(e.getMessage());
      LoggerManager.logInfo("Retry");
      return driver.findElement(By.cssSelector(css));
    }
  }

  public static WebElement findElementByClassname(String className) {
    try {
      LoggerManager.logFindElement("className", className);
      return driver.findElement(By.className(className));
    } catch (StaleElementReferenceException e) {
      LoggerManager.logError(e.getMessage());
      LoggerManager.logInfo("Retry");
      return driver.findElement(By.className(className));
    }
  }

  public static void inputData(WebElement element, String data) {
    LoggerManager.logInput(element, data);
    element.sendKeys(data);
  }

  public static void clickElement(WebElement element) {
    LoggerManager.logClick(element);
    element.click();
  }

  public static void selectDropdownByVisibleText(WebElement element, String option) {
    Select selectedElement = new Select(element);
    logSelect(element, "VisibleText: " + option);
    selectedElement.selectByVisibleText(option);
  }
}
