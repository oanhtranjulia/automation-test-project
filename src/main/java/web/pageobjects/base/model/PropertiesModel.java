package web.pageobjects.base.model;

/***
 * <p> Properties data class. </p>
 */
public class PropertiesModel {
  private String browser = null;
  private String url = null;
  private String username = null;
  private String password = null;
  private String headless = null;

  public PropertiesModel(String username, String password) {
    this.username = username;
    this.password = password;
  }

  /** PropertiesModel Constructor.
     *
     * @param browser property injected from .properties file
     * @param url property injected from .properties file
     * @param username property injected from .properties file
     * @param password property injected from .properties file
     * @param headless property injected from .properties file
     */
  public PropertiesModel(
      String browser, String url, String username, String password, String headless) {
    this.browser = browser;
    this.url = url;
    this.username = username;
    this.password = password;
    this.headless = headless;
  }

  public String getBrowser() {
    return browser;
  }

  public void setBrowser(String browser) {
    this.browser = browser;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getHeadless() {
    return headless;
  }

  public void setHeadless(String headless) {
    this.headless = headless;
  }
}
