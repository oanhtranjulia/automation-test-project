package web.pageobjects.inventorydetails;

import static web.pageobjects.base.model.SimplePage.findElementByCss;
import static web.utils.extensions.InventoryExtension.priceExtension;

import org.openqa.selenium.WebDriver;
import web.pageobjects.inventorydetails.model.InventoryDetailsModel;

/***
 * <p> InventoryDetailsPage page object. </p>
 */
public class InventoryDetailsPage {
  WebDriver driver;

  public InventoryDetailsPage(WebDriver driver) {
    this.driver = driver;
  }

  /***
   * <p> Get details of an Inventory Item at Inventory Details page. </p>
   *
   * <p>@return Inventory data class </p>
   */
  public InventoryDetailsModel getInventoryDetails() {
    InventoryDetailsModel inventoryDetailsModel = new InventoryDetailsModel();

    inventoryDetailsModel.name = getElementTextByCss(".inventory_details_name");
    inventoryDetailsModel.description = getElementTextByCss(".inventory_details_desc.large_size");
    inventoryDetailsModel.price = priceExtension(getElementTextByCss(".inventory_details_price"));

    return inventoryDetailsModel;
  }

  private String getElementTextByCss(String cssLocator) {
    return findElementByCss(cssLocator).getText();
  }
}
