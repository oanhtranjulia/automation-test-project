package web.pageobjects.inventorydetails.model;

/***
 * <p> Inventory Details data class. </p>
 */
public class InventoryDetailsModel {
  public String id;
  public String name;
  public String description;
  public Double price;
}
