package web.pageobjects.inventorylist.model;

/***
 * <p> Inventory Item List data class. </p>
 */
public class InventoryItemList {
  public String id;
  public String name;
  public String descritpion;
  public String price;
}
