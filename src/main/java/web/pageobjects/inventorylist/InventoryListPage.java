package web.pageobjects.inventorylist;

import static web.loggermanager.LoggerManager.logVerify;
import static web.pageobjects.base.model.SimplePage.clickElement;
import static web.pageobjects.base.model.SimplePage.findElementByClassname;
import static web.pageobjects.base.model.SimplePage.findElementByCss;
import static web.pageobjects.base.model.SimplePage.findElementByXpath;
import static web.pageobjects.base.model.SimplePage.selectDropdownByVisibleText;

import java.util.LinkedList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import web.pageobjects.inventorylist.model.InventoryItemList;

/***
 * <p> InventoryListPage page object. </p>
 */
public class InventoryListPage {
  WebDriver driver;

  String url = "https://www.saucedemo.com/inventory.html";

  public InventoryListPage(WebDriver driver) {
    this.driver = driver;
    driver.get(url);
  }

  public void clickOnInventoryItem(String inventoryName) {
    WebElement element = findElementByXpath(
        String.format("//*[text()='%s']", inventoryName));
    clickElement(element);
  }

  public boolean isInventoryListDisplayed() {
    boolean result = findElementByClassname("inventory_list").isDisplayed();
    logVerify("Is inventory list displayed?", result);
    return result;
  }

  /***
   * <p> Get all inventory details. </p>
   *
   * @return list of inventory details
   */
  public List<InventoryItemList> getInventoryList() {
    LinkedList<InventoryItemList> inventoryItemLists = new LinkedList<>();

    int noOfInventoryItems = driver.findElements(By.cssSelector(".inventory_item")).size();
    String nameXpath = "//*[@class='inventory_item'][%s]//*[@class='inventory_item_name ']";
    String descriptionXpath =
        "//*[@class='inventory_item'][%s]//*[@class='inventory_item_desc']";
    String priceXpath = "//*[@class='inventory_item'][%s]//*[@class='inventory_item_price']";

    for (int i = 0; i < noOfInventoryItems; i++) {
      InventoryItemList inventoryItemList = new InventoryItemList();
      inventoryItemList.name = getItemTextByXpath(nameXpath, i);
      inventoryItemList.descritpion = getItemTextByXpath(descriptionXpath, i);
      inventoryItemList.price = getItemTextByXpath(priceXpath, i);

      inventoryItemLists.add(inventoryItemList);
    }
    return inventoryItemLists;
  }

  private String getItemTextByXpath(String rawXpath, int i) {
    return findElementByXpath(String.format(rawXpath, i + 1)).getText();
  }

  public boolean isInventoryListNameOrderedAscending() {
    return isSortedAscendingDouble(getInventoryNames(), true);
  }

  public boolean isInventoryListNameOrderedDescending() {
    return isSortedAscendingDouble(getInventoryNames(), false);
  }

  private <T extends Comparable<T>> boolean isSortedAscendingDouble(
      LinkedList<T> inventoryItems, boolean isAscending) {
    boolean result = false;
    for (int i = 0; i < inventoryItems.size() - 1; i++) {
      result = isAscending == (compare(inventoryItems.get(i), inventoryItems.get(i + 1)) <= 0);
    }
    logVerify("Is the list sorted as ascending?", isAscending);
    return result;
  }

  private <T extends Comparable<T>> int compare(T obj1, T obj2) {
    return obj1.compareTo(obj2);
  }

  private LinkedList<String> getInventoryNames() {
    List<InventoryItemList> inventoryList = getInventoryList();
    LinkedList<String> inventoryNames = new LinkedList<>();

    for (InventoryItemList inventoryItemList : inventoryList) {
      inventoryNames.add(inventoryItemList.name);
    }
    return inventoryNames;
  }

  /***
   * <p>Select sort inventory options. </p>
   *
   * @param option sort options
   */
  public void sortInventoryList(String option) {
    selectDropdownByVisibleText(findElementByCss("select.product_sort_container"), option);
  }

  public boolean isInventoryListPriceOrderedAscending() {
    return isSortedAscendingDouble(getInventoryPrices(), true);
  }

  private LinkedList<Double> getInventoryPrices() {
    List<InventoryItemList> inventoryList = getInventoryList();
    LinkedList<Double> inventoryPrices = new LinkedList<>();

    for (InventoryItemList inventoryItemList : inventoryList) {
      inventoryPrices.add(Double.valueOf(inventoryItemList.price.substring(1)));
    }
    return inventoryPrices;
  }

  public boolean isInventoryListPriceOrderedDescending() {
    boolean sortedAscendingDouble = isSortedAscendingDouble(getInventoryPrices(), false);
    return sortedAscendingDouble;
  }
}