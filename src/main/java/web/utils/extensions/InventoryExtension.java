package web.utils.extensions;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** helper methods for Inventory Items. */
public class InventoryExtension {
  static final String PRICE_REGEX = "^([$])([0-9.+])(.*)";
  static final String INVENTORY_ID_ATTRIBUTE_REGEX = "^(item_)([0-9]+)(_title_link)$";

  /***
   * <p> Use regex to match the price without currency. </p>
   *
   *
   * @param priceString price extracted from the application
   * @return price
   */
  public static Double priceExtension(String priceString) {

    Pattern pattern = Pattern.compile(PRICE_REGEX);
    Matcher matcher = pattern.matcher(priceString);

    return matcher.find() ? Double.valueOf(matcher.group(2)) : 0.00;
  }

  /***
   * <p> Get the inventory item id from web element locator. </p>
   *
   * @param idAttribute inventory id item locator
   *
   * @return inventory item id
   */
  public static String inventoryIdExtension(String idAttribute) {
    Pattern pattern = Pattern.compile(INVENTORY_ID_ATTRIBUTE_REGEX);
    Matcher matcher = pattern.matcher(idAttribute);

    return matcher.find() ? matcher.group(2) : "";
  }
}
