package web.utils.extensions;

import io.qameta.allure.Allure;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/***
 * <p> Extension for Allure Report. </p>
 * */
public class AllureReportExtension {

  /***
   * <p> Add attachment to Allure report. </p>
   * */
  public static void addAttachement(String attachment) {

    String path = System.getProperty("user.dir") + "/src/test/resources/screenshots/" + attachment;
    Path content = Paths.get(path);
    try (InputStream is = Files.newInputStream(content)) {
      Allure.addAttachment("My attachment", is);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /***
   * <p> Add links to Allure Report. </p>
   * */
  public static void  addLinks(String... url) {
    for (int i = 0; i < url.length; i++) {
      Allure.link(url[i]);
    }
  }
}
