package web.utils.context;

import web.drivermanager.DriverManager;
import web.pageobjects.base.model.SimplePage;
import web.pageobjects.inventorydetails.InventoryDetailsPage;
import web.pageobjects.inventorylist.InventoryListPage;
import web.pageobjects.loginpage.LoginPage;

/***
 * <p> Driver setup and page object instantiate,
 * which follows cucumber-picocontainer dependencies injection. </p>
 */

public class Context extends DriverManager {
  SimplePage simplePage;
  InventoryDetailsPage inventoryDetailsPage;
  InventoryListPage inventoryListPage;
  LoginPage loginPage;

  public Context() {
    createDriver();
  }

  public SimplePage getSimplePage() {
    return simplePage == null ? new SimplePage(driver) : simplePage;
  }

  public InventoryListPage getInventoryListPage() {
    return inventoryListPage == null ? new InventoryListPage(driver) : inventoryListPage;
  }

  public LoginPage getLoginPage() {
    return loginPage == null ? new LoginPage(driver) : loginPage;
  }

  public InventoryDetailsPage getInventoryDetailsPage() {
    return inventoryDetailsPage == null
        ? new InventoryDetailsPage(driver) : inventoryDetailsPage;
  }
}
