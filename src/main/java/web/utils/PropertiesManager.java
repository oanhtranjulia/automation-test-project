package web.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import web.pageobjects.base.model.PropertiesModel;

/***
 * <p> Inject .properties to PropertiesModel data class. </p>
 */
public class PropertiesManager {
  private static final String CONFIG = "/src/test/resources/configs/web.prod.properties";

  /***
   * <p> Inject .properties to PropertiesModel data class. </p>
   *
   * @return PropertiesModel
   */
  public static PropertiesModel getProperties() {
    Properties properties = new Properties();
    PropertiesModel propertiesModel = null;

    InputStream inputStream = null;

    try {
      String currentDir = System.getProperty("user.dir");
      inputStream = new FileInputStream(currentDir + CONFIG);

      properties.load(inputStream);

      propertiesModel =
          new PropertiesModel(
              properties.getProperty("browser"),
              properties.getProperty("url"),
              properties.getProperty("username"),
              properties.getProperty("password"),
              properties.getProperty("headless"));

    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      try {
        if (inputStream != null) {
          inputStream.close();
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return propertiesModel;
  }
}
