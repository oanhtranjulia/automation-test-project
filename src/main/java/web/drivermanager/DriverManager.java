package web.drivermanager;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import web.loggermanager.LoggerManager;
import web.pageobjects.base.model.PropertiesModel;
import web.utils.PropertiesManager;

/***
 * <p> WebDriver Driver Factory. </p>
 */
public class DriverManager {

  public WebDriver driver;

  /***
   * <p> Create Specific WebDriver,
   * per browser properties from src/test/resources/configs/web.properties. </p>
   */
  public void createDriver() {
    PropertiesModel properties = PropertiesManager.getProperties();
    String browser = properties.getBrowser();

    switch (browser) {
      case "chrome":
        WebDriverManager.chromedriver().clearDriverCache().setup();
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments(properties.getHeadless());
        chromeOptions.addArguments("window-size=1920,1080");

        LoggerManager.logBrowserStart(browser);
        driver = new ChromeDriver(chromeOptions);
        break;
      case "firefox":
        WebDriverManager.firefoxdriver().clearDriverCache().setup();
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        firefoxOptions.addArguments(properties.getHeadless());

        LoggerManager.logBrowserStart(browser);
        driver = new FirefoxDriver(firefoxOptions);
        break;
      case "edge":
        WebDriverManager.edgedriver().clearDriverCache().setup();
        EdgeOptions edgeOptions = new EdgeOptions();
        edgeOptions.addArguments(properties.getHeadless());
        edgeOptions.addArguments("window-size=1920,1080");

        LoggerManager.logBrowserStart(browser);
        driver = new EdgeDriver(edgeOptions);
        break;
      case "safari":
        WebDriverManager.safaridriver().clearDriverCache().setup();
        SafariOptions safariOptions = new SafariOptions();

        LoggerManager.logBrowserStart(browser);
        driver = new SafariDriver(safariOptions);
        break;
      default:
        throw new IllegalStateException("Unexpected value: " + browser);
    }
  }

  /***
   *<p>Quit WebDriver (browser). </p>
   */
  public void closeDriver() {
    if (driver != null) {

      LoggerManager.logInfo("Quit browser.");
      driver.quit();
    }
  }
}
