package web.stepdefinitions.inventorydetails;

import static org.junit.Assert.assertEquals;
import static web.utils.extensions.InventoryExtension.priceExtension;

import io.cucumber.java.en.And;
import io.qameta.allure.Step;
import java.util.Map;
import web.pageobjects.inventorydetails.InventoryDetailsPage;
import web.pageobjects.inventorydetails.model.InventoryDetailsModel;
import web.utils.context.Context;

/***
 * <p> InventoryDetailsSteps step definition. </p>
 */
public class InventoryDetailsSteps {

  Context context;
  InventoryDetailsPage inventoryDetailsPage;

  public InventoryDetailsSteps(Context context) {
    inventoryDetailsPage = context.getInventoryDetailsPage();
  }

  @And("display correct Inventory Details:")
  @Step("display correct Inventory Details: {expectedData}")
  public void displayCorrectInventoryDetails(Map<String, String> expectedData) {
    InventoryDetailsModel actualInventoryDetails = inventoryDetailsPage.getInventoryDetails();

    assertEquals(
        "Is inventory name correct?", expectedData.get("name"), actualInventoryDetails.name);
    assertEquals(
        "Is inventory description correct?",
        expectedData.get("description"),
        actualInventoryDetails.description);
    assertEquals(
        "Is inventory price correct?",
        priceExtension(expectedData.get("price")),
        actualInventoryDetails.price);
  }
}
