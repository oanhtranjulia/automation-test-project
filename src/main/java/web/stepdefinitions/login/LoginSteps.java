package web.stepdefinitions.login;

import io.cucumber.java.en.Given;
import io.qameta.allure.Step;
import web.pageobjects.base.model.PropertiesModel;
import web.pageobjects.base.model.SimplePage;
import web.pageobjects.loginpage.LoginPage;
import web.utils.PropertiesManager;
import web.utils.context.Context;

/***
 * <p> All steps definition for Login page should be defined here. </p>
 */
public class LoginSteps {
  Context context;
  LoginPage loginPage;
  SimplePage simplePage;

  public LoginSteps(Context context) {
    this.context = context;
    loginPage = context.getLoginPage();
    simplePage = context.getSimplePage();
  }

  @Given("user is successfully login by config credentials to Swag Labs")
  @Step("user is successfully login by config credentials to Swag Labs")
  public void userIsLoggedToSwagLabs() {
    PropertiesModel propertiesModel = PropertiesManager.getProperties();
    loginPage.loginSwagLabs(propertiesModel.getUsername(), propertiesModel.getPassword());
  }
}
