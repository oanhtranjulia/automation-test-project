package web.stepdefinitions.inventory;

import static org.junit.Assert.assertTrue;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.qameta.allure.Step;
import web.pageobjects.inventorylist.InventoryListPage;
import web.utils.context.Context;

/***
 * <p> All steps definition for Inventory List page should be defined here. </p>
 */
public class InventoryListSteps {
  Context context;
  InventoryListPage inventoryListPage;

  public InventoryListSteps(Context context) {
    inventoryListPage = context.getInventoryListPage();
  }

  @When("user clicks on an Inventory item {string}")
  @Step("user clicks on an Inventory item {itemName}")
  public void userClicksOnAnInventoryItemSauceLabsBackpack(String itemName) {
    inventoryListPage.clickOnInventoryItem(itemName);
  }

  @Then("display Inventory Page with inventory list")
  @Step("display Inventory Page with inventory list")
  public void displayInventoryPageWithProducts() {
    assertTrue(
        "Is Inventory Page displayed with inventory list?",
        inventoryListPage.isInventoryListDisplayed());
  }

  @And("inventory list is sorted by Name \\(A to Z)")
  @Step("inventory list is sorted by Name \\(A to Z)")
  public void inventoryListIsShortedAsNameAscending() {
    assertTrue(
        "Is inventory list Sorted by Name \\(A to Z)?",
        inventoryListPage.isInventoryListNameOrderedAscending());
  }

  @When("user selects sort by {string}")
  @Step("user selects sort by {string}")
  public void userSelectsSortByPriceLowToHigh(String option) {
    inventoryListPage.sortInventoryList(option);
  }

  @Then("inventory list is sorted by Price \\(low to high)")
  @Step("inventory list is sorted by Price \\(low to high)")
  public void inventoryListIsSortedByPriceLowToHigh() {
    assertTrue(
        "Is inventory list sorted by Price \\(low to high)?",
        inventoryListPage.isInventoryListPriceOrderedAscending());
  }

  @Then("inventory list is sorted by Name \\(Z to A)")
  @Step("inventory list is sorted by Name \\(Z to A)")
  public void inventoryListIsSortedByNameDescending() {
    assertTrue(
        "Is inventory list sorted by Name \\(Z to A)?",
        inventoryListPage.isInventoryListNameOrderedDescending());
  }

  @Then("inventory list is sorted by Price \\(high to low)")
  @Step("inventory list is sorted by Price \\(high to low)")
  public void inventoryListIsSortedByPriceHighToLow() {
    assertTrue(
        "Is inventory list sorted by Price \\(high to low)?",
        inventoryListPage.isInventoryListPriceOrderedDescending());
  }
}
