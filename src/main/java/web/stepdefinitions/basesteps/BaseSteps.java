package web.stepdefinitions.basesteps;

import io.cucumber.java.en.Then;
import web.pageobjects.base.model.SimplePage;
import web.utils.context.Context;

/***
 * <p> All the common steps definition should be defined here. </p>
 */

public class BaseSteps {
  Context context;
  SimplePage simplePage;

  public BaseSteps(Context context) {
    this.context = context;
    simplePage = context.getSimplePage();
  }

  @Then("close the browser")
  public void close() {
    context.closeDriver();
  }
}
