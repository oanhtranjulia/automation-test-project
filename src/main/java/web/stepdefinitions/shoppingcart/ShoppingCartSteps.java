package web.stepdefinitions.shoppingcart;

import static org.junit.Assert.assertNotEquals;

import io.cucumber.java.en.Given;
import io.qameta.allure.Step;
import web.utils.extensions.AllureReportExtension;

public class ShoppingCartSteps {

  /**
   * <p> All steps definitions for Shopping Cart page should be defined here. </p>
   */

  @Given("failed test")
  @Step("failed test")
  public void failedStep() {
    AllureReportExtension.addAttachement("categories.json");
    AllureReportExtension.addLinks("https://docs.qameta.io/allure/", "https://www.saucedemo.com/");
    assertNotEquals("Test custom defects classification in Allure Report", 1, 1);
  }
}
