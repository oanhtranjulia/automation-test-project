# automation-test-project

## What Is This Project

automation-test-project is a small automation (test) project to showcase my skills to build
automation project, include
a simple CI/CD, from scratch. This is built with maven cucumber java junit4 with gitlab CI/CD and
gitlab Page.

## The Application Under Test

Demo Sauce Lab web page https://www.saucedemo.com/

## CI/CD

[Pipeline](https://gitlab.com/oanhtranjulia/automation-test-project/-/pipelines) is configuration
in [.gitlab-ci.yml](.gitlab-ci.yml) with 4 stages

- checkout
- build
- test
- report

Pipeline is triggered automatically once source code gets push to remote repository.

## Repository

https://gitlab.com/oanhtranjulia/automation-test-project

## Project Dependencies

- <maven.compiler.source>1.8</maven.compiler.source>
- <maven.compiler.target>1.8</maven.compiler.target>
- <aspectj.version>1.9.4</aspectj.version>
- <junit.version>4.11</junit.version>
- <allure-junit4.version>2.23.0</allure-junit4.version>
- <cucumber-java.version>7.12.1</cucumber-java.version>
- <cucumber-junit.version>7.12.1</cucumber-junit.version>
- <cucumber-picocontainer.version>7.12.1</cucumber-picocontainer.version>
- <selenium-java.version>4.9.1</selenium-java.version>
- <webdrivermanager.version>5.3.3</webdrivermanager.version>
- <allure-cucumber-jvm.version>2.19.0</allure-cucumber-jvm.version>
- <allure-attachments.version>2.13.8</allure-attachments.version>
- <maven-clean-plugin.version>3.1.0</maven-clean-plugin.version>
- <maven-resources-plugin.version>3.0.2</maven-resources-plugin.version>
- <maven-compiler-plugin.version>3.8.0</maven-compiler-plugin.version>
- <maven-surefire-plugin.version>2.22.1</maven-surefire-plugin.version>
- <allure-maven.version>2.10.0</allure-maven.version>
- <allure-maven.report.verion>2.14.0</allure-maven.report.verion>
- <maven-jar-plugin.version>3.0.2</maven-jar-plugin.version>
- <maven-install-plugin.version>2.5.2</maven-install-plugin.version>
- <maven-deploy-plugin.version>2.8.2</maven-deploy-plugin.version>
- <maven-site-plugin.version>3.7.1</maven-site-plugin.version>
- <maven-project-info-reports-plugin.version>3.0.0</maven-project-info-reports-plugin.version>
- <log4j-core.version>2.22.0</log4j-core.version>

## Style Guide
[intellij-java-google-style.xml](https://github.com/google/styleguide/blob/gh-pages/intellij-java-google-style.xml)

## How To Run Test On Gitlab CI/CD

- Pipeline is triggered automatically on target branch once source code is pushed to remote
  repository
- This can be performed manually by clicking button 'Run pipeline'
  from [Gitlab's Pipeline page](https://gitlab.com/oanhtranjulia/automation-test-project/-/pipelines)

### How To Access Test Report On Gitlab

Report artifacts are generated at report stage of pipeline. The artifacts expiry is 1 week to save
the storage.

In order to access the report, filtering success status of report job (
i.e. [success reports](https://gitlab.com/oanhtranjulia/automation-test-project/-/jobs?statuses=SUCCESS))
, then selects
the latest job id. From Job artifacts column at the right of job details page, you can either
Download or Browse to view
index.html by Gitlab Page (Browse -> allure-report -> index.html, click directly on the link to open
html page from your
browser).

## How To Run Test On Local

### This is assumed that you have these tools installed on your local machine.

- java sdk 11 or beyond with JAVA_HOME and add bin to the PATH
- Maven and add bin to the PATH
- Allure and add bin to the PATH
- IDE
- Git

### Run Test

- Clone the [Remote Repository](https://gitlab.com/oanhtranjulia/automation-test-project.git), note
  this is public
  repository hence no authenticate required to interact with it
- Change directory to root of the project
- Set browser and/or headless
  in [web.prod.properties](https://gitlab.com/oanhtranjulia/automation-test-project/-/blob/main/src/test/resources/configs/web.prod.properties)
- Run command line
    - To run whole project: mvn clean test
    - To run specific scenario's tag(s): mvn clean test -Dcucumber.filter.tags="@smoke_test", replace "@smoke_test" by your tag name

### Logging (view only)
Logging is configuration as INFO level for both Console and File Appenders ([log4j2.xml](https://gitlab.com/oanhtranjulia/automation-test-project/-/blob/main/src/main/resources/log4j2.xml))
- Console
- File: logs file are generated and grouped per day [logs](https://gitlab.com/oanhtranjulia/automation-test-project/-/blob/main/src/test/resources/logs) folder

### How To Access Test Report After Running Locally

Run command:

- To generate the report: allure generate ./target/allure-results --clean
- To serve the report: allure serve ./target/allure-results